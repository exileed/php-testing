FROM php:8.0-fpm-alpine3.15

MAINTAINER Dmitriy Kuts <me@exileed.com>

# Install dev dependencies
RUN apk add --no-cache --update --virtual .build-deps \
    $PHPIZE_DEPS \
    linux-headers \
    make \
    g++ \
    gcc \
    curl-dev \
    oniguruma-dev \
    pcre-dev \
    libc-dev \
    libpng-dev \
    icu-dev \
    libtool \
    libxml2-dev \
    postgresql-dev \
    sqlite-dev \
    rabbitmq-c-dev \
    imagemagick-dev \
    # Install production dependencies
 && apk add --update --no-cache \
    bash \
    curl \
    git \
    imagemagick \
    libgomp \
    musl \
    icu \
    mysql-client \
    openssh-client \
    postgresql-libs \
    rsync \
    rabbitmq-c \
    zlib-dev \
    libzip-dev \
    nodejs \
    zip \
    npm \
    yarn

#RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/v3.12/community/ gnu-libiconv=1.15-r2
#ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so

RUN pecl install -o -f amqp \
  && pecl install -o -f redis

# Install PECL and PEAR extensions
RUN yes | pecl install \
    imagick \
    xdebug
# Install and enable php extensions
RUN docker-php-ext-enable \
    imagick \
    redis \
    xdebug \
    amqp \
    && docker-php-ext-configure zip
RUN docker-php-ext-install \
    curl \
    mbstring \
    pdo \
    pdo_mysql \
    pdo_pgsql \
    pdo_sqlite \
    pcntl \
    tokenizer \
    xml \
    gd \
    bcmath \
    sockets \
    intl \
    exif \
    soap \
    zip

RUN echo 'memory_limit = 1G' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini \
    && echo 'xdebug.mode = coverage' >> /usr/local/etc/php/conf.d/xdebug.ini \
    && curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/*
