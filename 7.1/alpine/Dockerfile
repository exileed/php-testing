FROM php:7.1-fpm-alpine

# Install dev dependencies
RUN apk add --no-cache --update --virtual .build-deps \
    $PHPIZE_DEPS \
    make \
    g++ \
    gcc \
    curl-dev \
    oniguruma-dev \
    pcre-dev \
    libc-dev \
    libpng-dev \
    icu-dev \
    libtool \
    libxml2-dev \
    postgresql-dev \
    sqlite-dev \
    rabbitmq-c-dev \
    # Install production dependencies
 && apk add --update --no-cache \
    bash \
    curl \
    git \
    imagemagick \
    imagemagick-dev \
    libgomp \
    musl \
    icu \
    mysql-client \
    openssh-client \
    postgresql-libs \
    rsync \
    rabbitmq-c \
    zlib-dev \
    libzip-dev \
    nodejs \
    yarn

RUN pecl install -o -f amqp \
  && pecl install -o -f redis-4.3.0

# Install PECL and PEAR extensions
RUN yes | pecl install \
    imagick \
    xdebug-2.9.8

# Install and enable php extensions
RUN docker-php-ext-enable \
    imagick \
    redis \
    xdebug \
    amqp \
    && docker-php-ext-configure zip
RUN docker-php-ext-install \
    curl \
    iconv \
    mbstring \
    pdo \
    pdo_mysql \
    pdo_pgsql \
    pdo_sqlite \
    pcntl \
    tokenizer \
    xml \
    gd \
    bcmath \
    sockets \
    intl \
    exif \
    soap \
    zip

RUN echo 'memory_limit = 1G' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini \
    && curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* \
