<p align="center">
    <a href="https://www.docker.com/" target="_blank">
        <img src="https://www.docker.com/sites/default/files/mono_vertical_large.png" height="100px">
    </a>
    <h1 align="center">Docker php image for testing (CI)</h1>
    <br>
</p>

**Stable** 
[![pipeline status](https://gitlab.com/exileed/php-testing/badges/master/pipeline.svg)](https://gitlab.com/exileed/php-testing/commits/master)


## About

These Docker images are built on top of the official PHP Docker image (alpine), they contain additional PHP extensions required to testing (CI, but no code of the framework itself.
The `Dockerfile`(s) of this repository are designed to build from different PHP-versions by using *build arguments*.

### Available versions for `exileed/php-testing`

```
8.1, 8.0, 7.4, 7.3, 7.2, 7.0, 7.1, 5.6 
```

### Available arch
```
arm, arm64, 386, amd64
```

### Available extensions

```
amqp
curl
iconv
mbstring
pdo
pdo_mysql
pdo_pgsql
pdo_sqlite
redis
pcntl
tokenizer
xml
gd
bcmath
soap
sockets
intl
imagick
exif
xdebug
zip

```

### Available software

```
node.js
yarn
npm
composer
```


### Example build

```bash
docker buildx build --push --platform linux/arm/v7,linux/arm64/v8,linux/386,linux/amd64 -t xileed/php-testing:7.4  7.4/alpine
```

